import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, Subscription } from "rxjs";

@Injectable({
    providedIn: 'root',
})

export class AppService {
    isChecked: boolean=false;
    constructor() {
    }
    clickLable(val: any) {
        this.isChecked = val;
    }
}